/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  images: {
    domains: ["garlandtools.org"],
  },
};

module.exports = nextConfig;
