import { PrismaAdapter } from "@auth/prisma-adapter";
import { PrismaClient } from "@prisma/client";
import axios, { AxiosError } from "axios";
import NextAuth, { AuthOptions, TokenSet } from "next-auth";
import { Adapter } from "next-auth/adapters";
import { JWT } from "next-auth/jwt";
import KeycloakProvider from "next-auth/providers/keycloak";
import jwt_decode from "jwt-decode";

const prisma = new PrismaClient();
const prismaAdapter = PrismaAdapter(prisma);

const MyAdapter: Adapter = {
  ...prismaAdapter,
  linkAccount: (account) => {
    account["not_before_policy"] = account["not-before-policy"];
    delete account["not-before-policy"];
    return prismaAdapter.linkAccount!(account);
  },
};

const keycloak = KeycloakProvider({
  clientId: process.env.KEYCLOAK_ID!,
  clientSecret: process.env.KEYCLOAK_SECRET!,
  issuer: process.env.KEYCLOAK_ISSUER!,
});

export const authOptions: AuthOptions = {
  adapter: MyAdapter,
  session: {
    strategy: "jwt",
  },
  providers: [keycloak],
  callbacks: {
    async jwt({ token, account, user }) {
      if (account?.access_token) {
        token.provider = keycloak.id;
        token.access_token = account.access_token;
        token.refresh_token = account.refresh_token;
        token.id_token = account.id_token;
        token.expires_at = account.expires_at;
      }

      if (Date.now() < token.expires_at! * 1000) {
        return token;
      }

      try {
        const tokenEndpoint = `${keycloak.options?.issuer}/protocol/openid-connect/token`;
        const response = await fetch(tokenEndpoint, {
          body: new URLSearchParams({
            client_id: keycloak.options!.clientId,
            client_secret: keycloak.options!.clientSecret,
            grant_type: "refresh_token",
            refresh_token: token.refresh_token!,
          }),
          method: "POST",
        });

        const tokens: TokenSet = await response.json();

        if (!response.ok) throw tokens;

        var decoded = jwt_decode<any>(tokens.access_token!);

        token.expires_at = decoded.exp;
        token.id_token = tokens.id_token!;
        token.access_token = tokens.access_token!;
        token.refresh_token = tokens.refresh_token!;

        return token;
      } catch (error) {
        console.error("Error refreshing access token", error);
        return { ...token, error: "RefreshAccessTokenError" as const };
      }
    },
    async session({ session, token, user }) {
      session.accessToken = token.access_token;
      session.error = token.error;

      return session;
    },
  },
  events: {
    signOut: ({ session, token }) => doFinalSignoutHandshake(token),
  },
};
export default NextAuth(authOptions);

async function doFinalSignoutHandshake(jwt: JWT) {
  const { provider, id_token } = jwt;

  if (provider == keycloak.id && id_token) {
    try {
      const params = new URLSearchParams();
      params.append("id_token_hint", id_token);
      const { status, statusText } = await axios.get(
        `${
          keycloak.options?.issuer
        }/protocol/openid-connect/logout?${params.toString()}`
      );

      console.log("Completed post-logout handshake", status, statusText);
    } catch (e: any) {
      console.error(
        "Unable to perform post-logout handshake",
        (e as AxiosError)?.code || e
      );
    }
  }
}

declare module "next-auth" {
  interface Session {
    accessToken: string | undefined;
    error?: "RefreshAccessTokenError";
  }
}

declare module "next-auth/jwt" {
  interface JWT {
    access_token: string | undefined;
    id_token: string | undefined;
    provider: string;
    expires_at: number | undefined;
    refresh_token: string | undefined;
    error?: "RefreshAccessTokenError";
  }
}
