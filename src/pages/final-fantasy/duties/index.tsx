import Card from "@mui/joy/Card";
import Box from "@mui/joy/Box";
import AspectRatio from "@mui/joy/AspectRatio";
import Image from "next/image";
import { GetServerSideProps, InferGetServerSidePropsType } from "next";
import { getServerSession } from "next-auth";
import { authOptions } from "@/pages/api/auth/[...nextauth]";
import axios from "axios";
import DutyCard from "@/components/dutyCard";
import Link from "next/link";

interface Duty {
  dutyId: number;
  fullIcon: string;
  name: string;
}

export const getServerSideProps = (async (context) => {
  const session = await getServerSession(context.req, context.res, authOptions);

  try {
    const data = await axios.get<Array<Duty>>(
      "https://api.aesireanempire.com/duties",
      {
        headers: {
          Authorization: `Bearer ${session?.accessToken}`,
        },
      }
    );

    return { props: { duties: data.data } };
  } catch (error) {
    return { props: { duties: [] } };
  }
}) satisfies GetServerSideProps<{ duties: Array<Duty> }>;

export default function Home({
  duties,
}: InferGetServerSidePropsType<typeof getServerSideProps>) {
  return (
    <>
      <Box
        sx={{
          display: "flex",
          gap: 2,
          flexWrap: "wrap",
          margin: 2,
          justifyContent: "center",
        }}
      >
        {duties!.map((duty) => (
          <Link
            key={duty.dutyId}
            href={`/final-fantasy/duties/${duty.name.replace(" ", "_")}`}
          >
            <DutyCard key={duty.dutyId} duty={duty} />
          </Link>
        ))}
      </Box>
    </>
  );
}
