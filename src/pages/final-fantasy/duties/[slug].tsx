import {
  AspectRatio,
  CardContent,
  CardOverflow,
  Divider,
  Stack,
  Typography,
} from "@mui/joy";
import Card from "@mui/joy/Card";
import { useRouter } from "next/router";
import Image from "next/image";
import { GetServerSideProps, InferGetServerSidePropsType } from "next";
import DutyCard from "@/components/dutyCard";

interface Duty {
  dutyId: number;
  fullIcon: string;
  name: string;
  videos: Array<{
    dutyVideoId: number;
    dutyId: number;
    video: string;
  }>;
}

export const getServerSideProps = (async (context) => {
  return {
    props: {
      duty: {
        videos: [
          {
            dutyVideoId: 13,
            dutyId: 12,
            video: "www.youtube.com/watch?v=uXcb2GmzRso?",
          },
        ].map((video) => ({
          ...video,
          video: video.video.replace(
            "www.youtube.com/watch?v=",
            "//www.youtube.com/embed/"
          ),
        })),
        dutyId: 12,
        name: "Cutter's Cry",
        category: "Dungeons",
        patch: 2.0,
        level: 38,
        fullIcon: "https://garlandtools.org/files/icons/instance/112011.png",
        unlockedByQuest: "Dishonor Before Death",
      },
    },
  };
}) satisfies GetServerSideProps<{ duty: Duty }>;

export default function Duty({
  duty,
}: InferGetServerSidePropsType<typeof getServerSideProps>) {
  const router = useRouter();

  const slug = router.query?.slug as string | undefined;
  console.log(slug);
  if (slug?.includes(" ")) {
    router.replace("/final-fantasy/duties/" + slug.replace(" ", "_"));
  }

  return (
    <Stack
      spacing={4}
      sx={{
        display: "flex",
        maxWidth: "800px",
        mx: "auto",
        px: {
          xs: 2,
          md: 6,
        },
        py: {
          xs: 2,
          md: 3,
        },
      }}
    >
      <DutyCard duty={duty}>
        <CardContent>
          <Typography>Videos</Typography>
          <Divider></Divider>
          <iframe
            width="556"
            height="318"
            frameBorder="0"
            allowFullScreen={true}
            src={duty.videos[0].video}
          ></iframe>
        </CardContent>
      </DutyCard>
    </Stack>
  );
}
