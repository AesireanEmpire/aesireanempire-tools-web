import {
  Card,
  CardOverflow,
  AspectRatio,
  CardContent,
  Typography,
  Divider,
} from "@mui/joy";

import Image from "next/image";

interface Duty {
  dutyId: number;
  fullIcon: string;
  name: string;
}

interface DutyCardProps {
  duty: Duty;
}

export default function DutyCard({
  duty,
  children,
}: React.PropsWithChildren<DutyCardProps>) {
  return (
    <Card sx={{ width: 600 }} variant="outlined">
      <CardOverflow>
        <AspectRatio ratio="3">
          <Image
            src={duty.fullIcon}
            width="376"
            height="120"
            loading="lazy"
            alt={duty.name}
          />
        </AspectRatio>
      </CardOverflow>
      <CardContent>
        <Typography level="title-md">{duty.name}</Typography>
        {children}
      </CardContent>
    </Card>
  );
}
